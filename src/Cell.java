/**
 * @author Joel Olivieri (43623913) on 26/08/2018
 * @project Assignment 1
 */

import java.awt.*;

public class Cell {

    int x;
    int y;
    int grassHeight;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
        this.grassHeight = 0;
    }

    //Set grass height for instance of cell
    public void setGrassHeight(int height) {
        this.grassHeight = height;
    }

    public void paint(Graphics g, Boolean highlighted) {
        Color grass;

        //Choose red and blue colours based on intensity of green
        if (grassHeight >= 201 && grassHeight <= 250) {
            int rb = (int)(50 + Math.random() * (130 - 50));
            grass = new Color (rb , grassHeight, 0);
        }
        else if (grassHeight >= 151 && grassHeight <= 200) {
            int rb = (int)(Math.random() * 50 - 0);
            grass = new Color (rb , grassHeight, rb);
        }
        else {
            grass = new Color(0, grassHeight, 0);
        }

        //Draw grass cells
        g.setColor(grass);
        g.fillRect(x, y, 35, 35);
        g.setColor(Color.black);
        g.drawRect(x, y, 35, 35);

        //If highlighted, outline cell in grey
        if (highlighted) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawRect(x + 1, y + 1, 33, 33);
        }
    }

    public boolean contains(Point target){
        if (target == null)
            return false;
        return target.x > x && target.x < x + 35 && target.y > y && target.y < y +35;
    }
}