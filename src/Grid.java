/**
 * @author Joel Olivieri (43623913) on 26/08/2018
 * @project Assignment 1
 */

import java.awt.*;

public class Grid {

    private Cell[][] cells = new Cell[20][20];

    private int x;
    private int y;
    private Point lastPosition; //Last mouse position
    private int toolTipCount = 0; //Tool Tip Timer

    public Grid(int x, int y) {
        this.x = x;
        this.y = y;

        //Create grid of cells
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                //Create new instance of cell and randomise grass height
                cells[i][j] = new Cell(x + j * 35, y + i * 35);
                cells[i][j].setGrassHeight(40 + (int) (Math.random() * (255 - 40)));
            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        Boolean drawToolTip = false;
        String toolTip = "Grass Height: ";
        int grassHeight = 0;
        int toolTipDelay = 1750;

        //Stops null pointer exceptions
        if (mousePosition != null && lastPosition != null) {
            //If mouse still, increment timer
            if (lastPosition.equals(mousePosition)) {
                toolTipCount ++;
                //If mouse still for delay amount, draw tool tip
                if (toolTipCount >= toolTipDelay) {
                    drawToolTip = true;
                }
            }
            else {
                toolTipCount = 0;
            }
        }
        else {
            toolTipCount = 0;
        }

        //Draw grid of cells
        for (int y = 0; y < 20; ++y) {
            for (int x = 0; x < 20; ++x) {
                Cell thisCell = cells[x][y];
                thisCell.paint(g, thisCell.contains(mousePosition));
                //If mouse in cell, get grass height of cell
                if (thisCell.contains(mousePosition)) {
                    grassHeight = thisCell.grassHeight;
                }
            }
        }

        //If ready to draw tool tip and cell's grass height does not equal 0 (outside grid), draw tool tip
        if (drawToolTip && grassHeight != 0) {
            g.setColor(Color.yellow);
            g.fillRect(mousePosition.x, mousePosition.y - 13, 100, 17);
            g.setColor(Color.black);
            g.drawString(toolTip + Integer.toString(grassHeight/50) + "m", mousePosition.x, mousePosition.y);
        }

        //Update last mouse position
        lastPosition = mousePosition;
    }
}